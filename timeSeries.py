##########
# IMPORT #
##########

import pandas as pd
import numpy as np
import os, csv, json
import matplotlib.pyplot as plt
import glob
import seaborn as sns
from sklearn.metrics import r2_score, median_absolute_error, mean_absolute_error, mean_absolute_percentage_error
from sklearn.metrics import median_absolute_error, mean_squared_error, mean_squared_log_error
from scipy.optimize import minimize
import statsmodels.tsa.api as smt
import statsmodels.api as sm
from tqdm import tqdm_notebook
from itertools import product
import shutil
import datetime
from statistics import mean, stdev
from sktime.forecasting.arima import AutoARIMA
from statsmodels.tsa.statespace.sarimax import SARIMAX
from kats.consts import TimeSeriesData
from kats.utils.decomposition import TimeSeriesDecomposition

#############################
# Dataframe Display Setting #
#############################

desired_width=320
pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns',25)

##########
# Global #
##########

# ---------------------------------------------------
# The features (measures) of the project history data
# ---------------------------------------------------
features = ['issue_comments', 'commit_comments', 'issues', 'watchers',
       'closePR', 'commits', 'committers', 'openPR']


# ---------------------------------------------------
# Build the list of dates considered
# ---------------------------------------------------
thestart = pd.to_datetime('1999-07-01')
theend = pd.to_datetime('2019-06-30')
date_list = [(pd.to_datetime('1999-07-01') + datetime.timedelta(days=x)).date() for x in range((theend - thestart).days+1)]
date_list = [str(x) for x in date_list]

# ---------------------------------------------------
# Build the list of project_id selected
# ---------------------------------------------------

df_temp = pd.read_csv('oss_selected.csv')
projectList = df_temp.loc[:,'id'].values.tolist()

# -------------------------------------------------
# Read the list of 'active' and 'inactive' projects
# -------------------------------------------------
with open('active.txt', 'r', encoding='utf-8') as txtfile:
    activeList = [x.strip('\n') for x in txtfile.readlines()]
with open('inactive.txt', 'r', encoding='utf-8') as txtfile:
    inactiveList = [x.strip('\n') for x in txtfile.readlines()]

#############
# Functions #
#############

def selectOnesExisting(osslistfile):
    # -----------------------------------------------------
    # get the list of the projects whose data already exist
    # copy to a testing folder
    # -----------------------------------------------------
    df_selected = pd.read_csv(osslistfile)
    idlist = df_selected.loc[:,'id'].values.tolist()
    filelist = glob.glob('csvByProject/*.csv')
    filelist = [int(x.split('/')[1].split('.')[0]) for x in filelist]
    existing = [x for x in filelist if x in idlist]
    existingfiles = ['csvByProject/'+str(x)+'.csv' for x in existing]
    for file in existingfiles:
        shutil.copyfile(file, 'testing/'+file.split('/')[1])

def makeTimeSeriesData(project_id):
    # -----------------------------------------------------------------
    # Shape the TS data from the project's first commit till 2018-12-31
    # -----------------------------------------------------------------
    thedf = pd.read_csv(f'testing/{project_id}.csv')
    firstcommit = pd.to_datetime(thedf.loc[thedf['commits'] != 0, 'date'].values[0])
    thedf['date'] = pd.to_datetime(thedf['date']).dt.date
    thedf_active = thedf.loc[(thedf['date']>=firstcommit) & (thedf['date']<=pd.to_datetime('2018-12-31')), ['date']+features]
    thedf_active.set_index('date', inplace=True)
    return  thedf_active

def showplotfor1feature(project_id, feature):
    # -----------------------------------------------------
    # Plot the Figure for Project on 1 feature/measure
    # -----------------------------------------------------
    df = makeTimeSeriesData(project_id)
    plt.figure(figsize=(17, 8))
    plt.plot(df[feature])
    plt.title(f'{feature} of the Project {project_id}')
    plt.ylabel(f'Number of {feature}')
    plt.xlabel('Date')
    plt.grid(False)
    plt.show()

def plot_moving_average(series, window, plot_intervals=False, scale=1.96):
    # -----------------------------------------------------
    # Plot the moving average
    # -----------------------------------------------------
    rolling_mean = series.rolling(window=window).mean()
    plt.figure(figsize=(17, 8))
    plt.title('Moving average\n window size = {}'.format(window))
    plt.plot(rolling_mean, 'g', label='Rolling mean trend')
    # Plot confidence intervals for smoothed values
    if plot_intervals:
        mae = mean_absolute_error(series[window:], rolling_mean[window:])
        deviation = np.std(series[window:] - rolling_mean[window:])
        lower_bound = rolling_mean - (mae + scale * deviation)
        upper_bound = rolling_mean + (mae + scale * deviation)
        plt.plot(upper_bound, 'r--', label='Upper bound / Lower bound')
        plt.plot(lower_bound, 'r--')

    plt.plot(series[window:], label='Actual values')
    plt.legend(loc='best')
    plt.grid(True)
    plt.show()

def exponential_smoothing(series, alpha):
    result = [series[0]]  # first value is same as series
    for n in range(1, len(series)):
        result.append(alpha * series[n] + (1 - alpha) * result[n - 1])
    return result

def plot_exponential_smoothing(series, alphas):
    plt.figure(figsize=(17, 8))
    for alpha in alphas:
        plt.plot(exponential_smoothing(series, alpha), label="Alpha {}".format(alpha))
    plt.plot(series.values, "c", label="Actual")
    plt.legend(loc="best")
    plt.axis('tight')
    plt.title("Exponential Smoothing")
    plt.grid(True)
    plt.show()

def double_exponential_smoothing(series, alpha, beta):
    result = [series[0]]
    for n in range(1, len(series) + 1):
        if n == 1:
            level, trend = series[0], series[1] - series[0]
        if n >= len(series):  # forecasting
            value = result[-1]
        else:
            value = series[n]
        last_level, level = level, alpha * value + (1 - alpha) * (level + trend)
        trend = beta * (level - last_level) + (1 - beta) * trend
        result.append(level + trend)
    return result

def plot_double_exponential_smoothing(series, alphas, betas):
    plt.figure(figsize=(17, 8))
    for alpha in alphas:
        for beta in betas:
            plt.plot(double_exponential_smoothing(series, alpha, beta), label="Alpha {}, beta {}".format(alpha, beta))
    plt.plot(series.values, label="Actual")
    plt.legend(loc="best")
    plt.axis('tight')
    plt.title("Double Exponential Smoothing")
    plt.grid(True)
    plt.show()

def tsplot(y, lags=None, figsize=(12, 7), syle='bmh'):
    if not isinstance(y, pd.Series):
        y = pd.Series(y)

    with plt.style.context(style='bmh'):
        fig = plt.figure(figsize=figsize)
        layout = (2, 2)
        ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
        acf_ax = plt.subplot2grid(layout, (1, 0))
        pacf_ax = plt.subplot2grid(layout, (1, 1))

        y.plot(ax=ts_ax)
        p_value = sm.tsa.stattools.adfuller(y)[1]
        ts_ax.set_title('Time Series Analysis Plots\n Dickey-Fuller: p={0:.5f}'.format(p_value))
        smt.graphics.plot_acf(y, lags=lags, ax=acf_ax)
        smt.graphics.plot_pacf(y, lags=lags, ax=pacf_ax)
        plt.tight_layout()
        plt.show()

def checkDiff(project_id, measure, diff):
    # ------------------------------------------------------------
    # to get rid of the high autocorrelation and
    # to make the process stationary. We take the first difference
    # ------------------------------------------------------------
    testing_df=makeTimeSeriesData(project_id)
    data_diff = testing_df[measure] - testing_df[measure].shift(diff)
    # ------------------------------------------------------------
    # See the stationary plot with lag 1 day
    # -------------------------------------------------------------
    tsplot(data_diff[diff:], lags=30)

# --------------------------------------------------------------------
# Testing Project https://api.github.com/repos/angular/angular.js (37)

#testing_df = makeTimeSeriesData(37)

# Show the plot for Angular.js Commits
#showplotfor1feature(37, 'commits')
# --------------------------------------------------------------------

#plot_exponential_smoothing(testing_df['commits'], [0.05, 0.3])
#plot_double_exponential_smoothing(testing_df['commits'], alphas=[0.9, 0.02], betas=[0.9, 0.02])

# ------------------------------------------------------------
# to get rid of the high autocorrelation and
# to make the process stationary. We take the first difference
# ------------------------------------------------------------

#data_diff = testing_df['commits'] - testing_df['commits'].shift(1)

# ------------------------------------------------------------
# See the stationary plot with lag 1 day
#-------------------------------------------------------------
#tsplot(data_diff[1:], lags=30)

# -----------------------------------------------------------
# Train many SARIMA models to find the best set of parameters
# -----------------------------------------------------------

def optimize_SARIMA(inputSeries, parameters_list, d, D, s):
    # ---------------------------------------------------------
    #    Return dataframe with parameters and corresponding AIC

    #    parameters_list - list with (p, q, P, Q) tuples
    #    d - integration order
    #    D - seasonal integration order
    #    s - length of season
    # ---------------------------------------------------------
    ###Normalize###
    #thenorm = df['commits'] / (df['commits'].max() - df['commits'].min())
    ###############
    results = []
    best_aic = float('inf')
    for param in tqdm_notebook(parameters_list):
        try:
            model = sm.tsa.statespace.SARIMAX(inputSeries, order=(param[0], d, param[1]),
                                              seasonal_order=(param[2], D, param[3], s)).fit(disp=-1)
        except:
            continue

        aic = model.aic
        # Save best model, AIC and parameters
        if aic < best_aic:
            best_model = model
            best_aic = aic
            best_param = param
        results.append([param, model.aic])

    result_table = pd.DataFrame(results)
    result_table.columns = ['parameters', 'aic']
    # Sort in ascending order, lower AIC is better
    result_table = result_table.sort_values(by='aic', ascending=True).reset_index(drop=True)

    return result_table

def predictionPlot(forecast, original):
    forecastfilled = forecast
    #forecastfilled = [-0.02] * 1699 + forecast.values.tolist()
    #forecastfilled = pd.Series(forecastfilled[-200:-60])
    comparison = pd.DataFrame({'actual': original,
                               'predicted': forecastfilled})

    # Plot predicted vs actual price

    plt.figure(figsize=(17, 8))
    plt.plot(comparison.actual)
    plt.plot(comparison.predicted)
    plt.title('Compare')
    plt.ylabel('Commits')
    plt.xlabel('day')
    plt.legend(loc='best')
    plt.grid(False)
    plt.show()


# -----------------------------------------------------------
# Get the forecasting data for a project(using project_id) on
# all 8 measure, and output a csv
# -----------------------------------------------------------
def getForecastForOneYearByMonth(project_id):
    # make dataset
    testing_df = makeTimeSeriesData(project_id)
    # ----------------------------------
    # Use CheckDiff function to
    # set initial values and some bounds
    # ----------------------------------
    ps = range(0, 2)
    d = 1
    qs = range(0, 2)
    Ps = range(0, 2)
    D = 1
    Qs = range(0, 2)
    s = 7
    # ---------------------------------
    # Create combinations of parameters
    # ---------------------------------
    parameters = product(ps, qs, Ps, Qs)
    parameters_list = list(parameters)
    theforcastdict = {}
    for measure in features:
        result_table = optimize_SARIMA(testing_df[measure], parameters_list, d, D, s)
        # Set parameters that give the lowest AIC (Akaike Information Criteria)
        p, q, P, Q = result_table.parameters[0]
        best_model = sm.tsa.statespace.SARIMAX(testing_df[measure], order=(p, d, q),
                                               seasonal_order=(P, D, Q, s)).fit(disp=-1)
        # forecast the next year (2019-01-01 to 2019-12-31 here)
        forecast = best_model.predict(start=testing_df[measure].shape[0], end=testing_df[measure].shape[0] + 364)
        df_f = forecast.to_frame()
        df_f.reset_index(inplace=True)
        df_f.columns = ['date', 'values']

        def getmonth(datestr):
            return int(datestr.month)
            # return int(str(datestr.split('-')[1])

        df_f['month'] = df_f['date'].apply(getmonth)
        monthlist = []
        for i in range(1,13):
            monthnumbers = df_f.loc[df_f['month']==i, 'values'].values.tolist()
            themean = mean(monthnumbers)
            if themean<0:
                monthlist.append(0)
            else:
                monthlist.append(round(themean))
        theforcastdict[measure] = monthlist
    thedf = pd.DataFrame.from_dict(theforcastdict)
    thedf['month'] = ['2019-'+str(x) for x in range(1,13)]
    thedf.to_csv(f"tsoutput/output_ts_{project_id}.csv", index=False)
    print(thedf)



# -----------------------------------------------------------
# make day-based dataset for each project on 8 measures
# save in the testing folder
# -----------------------------------------------------------
def makeCSVbyProject(project_id):
    allMeasures = glob.glob("selected/*_selected.csv")
    df_new = pd.DataFrame.from_dict({'date': date_list})
    for filename in allMeasures:
        df = pd.read_csv(filename, index_col=None)
        df = df.loc[df['project_id']==project_id, :]
        df_new = pd.merge(df_new, df.iloc[:,1:3], how='outer', on='date')

    df_new.fillna(0, inplace=True)
    df_new.to_csv(f'testing/{project_id}.csv',index=False)




####################
# The Testing Area #
####################

#result_table = optimize_SARIMA(parameters_list, d, D, s)
#print(result_table)
#plot_moving_average(testing_df['commits'], 30)

#testing_df.reset_index(inplace=True)
#testing_df = testing_df.loc[:, ['date', 'commits']]
#testing_df = testing_df.rename(columns={'date': 'time', 'commits': 'value'})
#ts = TimeSeriesData(testing_df)
#decomposer = TimeSeriesDecomposition(ts, decomposition='additive')
#results = decomposer.decomposer()
#fig = decomposer.plot()
#plt.show()

#result_table = optimize_SARIMA(parameters_list, d, D, s)
# Set parameters that give the lowest AIC (Akaike Information Criteria)
#p, q, P, Q = result_table.parameters[0]
#best_model = sm.tsa.statespace.SARIMAX(testing_df['commits'], order=(p, d, q),
#                                       seasonal_order=(P, D, Q, s)).fit(disp=-1)
#print(best_model.summary())

#forecast = best_model.predict(start=testing_df['commits'].shape[0]-89, end=testing_df['commits'].shape[0])
#forecast_normalback = forecast*(df['commits'].max() - df['commits'].min())
#original = testing_df['commits'].values.tolist()[-90:]

#predictionPlot(forecast, original)

#forecast = best_model.predict(start=testing_df['commits'].shape[0], end=testing_df['commits'].shape[0]+364)
#print(len(forecast))
#print(forecast)

#allfiles = glob.glob('testing/*.csv')
#allfiles = [int(x.split('.')[0].split('/')[1]) for x in allfiles]
#for file in allfiles:
#    getForecastForOneYearByMonth(file)

#allfiles = glob.glob('testing/*.csv')
#allfiles = [int(x.split('.')[0].split('/')[1]) for x in allfiles]
#newprojects = [x for x in projectList if x not in allfiles]
#for item in newprojects:
#    makeCSVbyProject(item)
#    print(item)

#existing = glob.glob('tsoutput/*.csv')
#existing = [int(x.split('_')[-1].split('.')[0]) for x in existing]
#newprojects = [x for x in projectList if x not in existing]
#for item in newprojects:
#    try:
#        print(item)
#        getForecastForOneYearByMonth(item)
#    except AttributeError:
#        continue