from pprint import pprint
import re
import time
import pandas as pd
import numpy as np
from glob import glob
import os, csv, json
import matplotlib.pyplot as plt
from scipy import stats
from scipy.stats import kstest, norm
from scipy.stats import shapiro
from scipy.stats import normaltest
from scipy.stats import anderson
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from datetime import date
from sklearn.metrics import classification_report
from matplotlib.ticker import NullFormatter
import matplotlib.ticker as ticker
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn import svm, datasets
from sklearn.naive_bayes import GaussianNB
from datetime import timedelta

from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf


import seaborn as sns
sns.set()

from sklearn.metrics import r2_score, median_absolute_error, mean_absolute_error, mean_absolute_percentage_error
from sklearn.metrics import median_absolute_error, mean_squared_error, mean_squared_log_error

from scipy.optimize import minimize
import statsmodels.tsa.api as smt
import statsmodels.api as sm

from tqdm import tqdm_notebook

from itertools import product

#def mean_absolute_percentage_error(y_true, y_pred):
#    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

features = ['issue_comments', 'commit_comments', 'issues', 'watchers',
       'closePR', 'commits', 'committers', 'openPR']

with open('active.txt', 'r', encoding='utf-8') as txtfile:
    activeList = [x.strip('\n') for x in txtfile.readlines()]
with open('inactive.txt', 'r', encoding='utf-8') as txtfile:
    inactiveList = [x.strip('\n') for x in txtfile.readlines()]

def makeTimeSeriesData(project_id):
    thedf = pd.read_csv(f'csvByProject/{project_id}.csv')
    firstcommit = pd.to_datetime(thedf.loc[thedf['commits'] != 0, 'date'].values[0])
    thedf['date'] = pd.to_datetime(thedf['date']).dt.date
    thedf_active = thedf.loc[(thedf['date']>=firstcommit) & (thedf['date']<pd.to_datetime('2018-12-01')), ['date']+features]
    thedf_active.set_index('date', inplace=True)
    return  thedf_active

def showplotfor1feature(project_id, feature):
    df = makeTimeSeriesData(project_id)
    plt.figure(figsize=(17, 8))
    plt.plot(df[feature])
    plt.title(f'{feature} of the Project {project_id}')
    plt.ylabel(f'Number of {feature}')
    plt.xlabel('Date')
    plt.grid(False)
    plt.show()

def plot_moving_average(series, window, plot_intervals=False, scale=1.96):
    rolling_mean = series.rolling(window=window).mean()

    plt.figure(figsize=(17, 8))
    plt.title('Moving average\n window size = {}'.format(window))
    plt.plot(rolling_mean, 'g', label='Rolling mean trend')

    # Plot confidence intervals for smoothed values
    if plot_intervals:
        mae = mean_absolute_error(series[window:], rolling_mean[window:])
        deviation = np.std(series[window:] - rolling_mean[window:])
        lower_bound = rolling_mean - (mae + scale * deviation)
        upper_bound = rolling_mean + (mae + scale * deviation)
        plt.plot(upper_bound, 'r--', label='Upper bound / Lower bound')
        plt.plot(lower_bound, 'r--')

    plt.plot(series[window:], label='Actual values')
    plt.legend(loc='best')
    plt.grid(True)
    plt.show()

def exponential_smoothing(series, alpha):
    result = [series[0]]  # first value is same as series
    for n in range(1, len(series)):
        result.append(alpha * series[n] + (1 - alpha) * result[n - 1])
    return result


def plot_exponential_smoothing(series, alphas):
    plt.figure(figsize=(17, 8))
    for alpha in alphas:
        plt.plot(exponential_smoothing(series, alpha), label="Alpha {}".format(alpha))
    plt.plot(series.values, "c", label="Actual")
    plt.legend(loc="best")
    plt.axis('tight')
    plt.title("Exponential Smoothing")
    plt.grid(True)
    plt.show()

def double_exponential_smoothing(series, alpha, beta):
    result = [series[0]]
    for n in range(1, len(series) + 1):
        if n == 1:
            level, trend = series[0], series[1] - series[0]
        if n >= len(series):  # forecasting
            value = result[-1]
        else:
            value = series[n]
        last_level, level = level, alpha * value + (1 - alpha) * (level + trend)
        trend = beta * (level - last_level) + (1 - beta) * trend
        result.append(level + trend)
    return result


def plot_double_exponential_smoothing(series, alphas, betas):
    plt.figure(figsize=(17, 8))
    for alpha in alphas:
        for beta in betas:
            plt.plot(double_exponential_smoothing(series, alpha, beta), label="Alpha {}, beta {}".format(alpha, beta))
    plt.plot(series.values, label="Actual")
    plt.legend(loc="best")
    plt.axis('tight')
    plt.title("Double Exponential Smoothing")
    plt.grid(True)
    plt.show()

def tsplot(y, lags=None, figsize=(12, 7), syle='bmh'):
    if not isinstance(y, pd.Series):
        y = pd.Series(y)

    with plt.style.context(style='bmh'):
        fig = plt.figure(figsize=figsize)
        layout = (2, 2)
        ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
        acf_ax = plt.subplot2grid(layout, (1, 0))
        pacf_ax = plt.subplot2grid(layout, (1, 1))

        y.plot(ax=ts_ax)
        p_value = sm.tsa.stattools.adfuller(y)[1]
        ts_ax.set_title('Time Series Analysis Plots\n Dickey-Fuller: p={0:.5f}'.format(p_value))
        smt.graphics.plot_acf(y, lags=lags, ax=acf_ax)
        smt.graphics.plot_pacf(y, lags=lags, ax=pacf_ax)
        plt.tight_layout()
        plt.show()


#df = makeTimeSeriesData('10039226')
df = makeTimeSeriesData('114')
thenorm = df['commits'] / (df['commits'].max() - df['commits'].min())

#plot_exponential_smoothing(df['commits'], [0.05, 0.3])
#plot_double_exponential_smoothing(df['commits'], alphas=[0.9, 0.02], betas=[0.9, 0.02])
#tsplot(df['commits'], lags=30)

# Take the first difference to remove to make the process stationary
#data_diff = df['commits'] - df['commits'].shift(1)
#tsplot(data_diff[1:], lags=30)

# Set initial values and some bounds

ps = range(0, 5)
d = 1
qs = range(0, 5)
Ps = range(0, 5)
D = 1
Qs = range(0, 5)
s = 5

parameters = product(ps, qs, Ps, Qs)
parameters_list = list(parameters)
len(parameters_list)

# Train many SARIMA models to find the best set of parameters
def optimize_SARIMA(parameters_list, d, D, s):
    """
        Return dataframe with parameters and corresponding AIC

        parameters_list - list with (p, q, P, Q) tuples
        d - integration order
        D - seasonal integration order
        s - length of season
    """

    ###Normalize###
    #thenorm = df['commits'] / (df['commits'].max() - df['commits'].min())
    ###############


    results = []
    best_aic = float('inf')

    for param in tqdm_notebook(parameters_list):
        try:
            model = sm.tsa.statespace.SARIMAX(df['commits'], order=(param[0], d, param[1]),
                                              seasonal_order=(param[2], D, param[3], s)).fit(disp=-1)
        except:
            continue

        aic = model.aic

        # Save best model, AIC and parameters
        if aic < best_aic:
            best_model = model
            best_aic = aic
            best_param = param
        results.append([param, model.aic])

    result_table = pd.DataFrame(results)
    result_table.columns = ['parameters', 'aic']
    # Sort in ascending order, lower AIC is better
    result_table = result_table.sort_values(by='aic', ascending=True).reset_index(drop=True)

    return result_table


#result_table = optimize_SARIMA(parameters_list, d, D, s)
# Set parameters that give the lowest AIC (Akaike Information Criteria)
#p, q, P, Q = result_table.parameters[0]

p, d, q = 0,1,1
P, D, Q, s = 3,1,1,5
#p, d, q = 4,1,4
#P, D, Q, s = 4,1,4,5

best_model = sm.tsa.statespace.SARIMAX(thenorm, order=(p, d, q),
                                       seasonal_order=(P, D, Q, s)).fit(disp=-1)

print(best_model.summary())
forecast = best_model.predict(start=df.shape[0]-89, end=df.shape[0])
#forecast_normalback = forecast*(df['commits'].max() - df['commits'].min())
original = df['commits'].values.tolist()[-90:]

def predictionPlot(forecast, original):
    forecastfilled = forecast
    #forecastfilled = [-0.02] * 1699 + forecast.values.tolist()
    #forecastfilled = pd.Series(forecastfilled[-200:-60])
    comparison = pd.DataFrame({'actual': original,
                               'predicted': forecastfilled})

    # Plot predicted vs actual price

    plt.figure(figsize=(17, 8))
    plt.plot(comparison.actual)
    plt.plot(comparison.predicted)
    plt.title('Compare')
    plt.ylabel('Commits')
    plt.xlabel('day')
    plt.legend(loc='best')
    plt.grid(False)
    plt.show()

predictionPlot(forecast, original)

#thelist = []
#for i in range(len(original)-1):
    #print([round(x,2) for x in forecast_normalback.tolist()])
    #print(original)
#    print(mean_absolute_percentage_error(forecast_normalback[:i+1], original[:i+1]))
#    thelist.append(mean_absolute_percentage_error(forecast_normalback[:i+1], original[:i+1]))
#print(min(thelist))