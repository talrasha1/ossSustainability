##########
# IMPORT #
##########

import pandas as pd
import numpy as np
import os, csv, json
import matplotlib.pyplot as plt
import glob
import seaborn as sns
from sklearn.metrics import r2_score, median_absolute_error, mean_absolute_error, mean_absolute_percentage_error
from sklearn.metrics import median_absolute_error, mean_squared_error, mean_squared_log_error
from scipy.optimize import minimize
import statsmodels.tsa.api as smt
import statsmodels.api as sm
from tqdm import tqdm_notebook
from itertools import product
import shutil
from statistics import stdev, mean
from sktime.forecasting.arima import AutoARIMA
from statsmodels.tsa.statespace.sarimax import SARIMAX
from kats.consts import TimeSeriesData
from kats.utils.decomposition import TimeSeriesDecomposition

#############################
# Dataframe Display Setting #
#############################

desired_width=320
pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns',25)

##########
# Global #
##########


#############
# Functions #
#############

def getLastMonthAverageMeasure(measure):
    # -----------------------------------------------------
    # get the list of last month average for the measure
    # of all the selected projects
    # -----------------------------------------------------
    lastmonthAVGlist = []
    allfilesIncsvByProject = glob.glob('csvByProject/*.csv')
    for file in allfilesIncsvByProject:
        df_temp = pd.read_csv(file)
        lastmonthlist = df_temp.loc[(df_temp['date']>='2019-05-01') & (df_temp['date']<='2019-05-31'), measure].values.tolist()
        lastmonthAVGlist.append(sum(lastmonthlist)/len(lastmonthlist))
    return lastmonthAVGlist

def positionTheTargetProject(lastmonthAVGlist, project_id):
    themean = mean(lastmonthAVGlist)
    sigma = stdev(lastmonthAVGlist)
