import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
from datetime import date
import seaborn as sns

import scipy.stats as st
from scipy.stats import shapiro
from scipy.stats import normaltest
from scipy.stats import anderson
from scipy import stats
import statsmodels.api as sm
from scipy.stats import norm
import pylab
from scipy import stats
from scipy.stats import kstest, norm
from scipy.stats import shapiro
from scipy.stats import normaltest
from scipy.stats import anderson
from scipy import stats
from scipy.stats import norm
import pylab
from scipy.interpolate import interp1d
import math
from numpy import mean
from numpy import std


np.set_printoptions(linewidth=320)
pd.set_option('display.width', 320)
pd.set_option('display.max_columns',25)
distributionString = "st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,st.dgamma,st.dweibull,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy"
dist_names = [x[3:] for x in distributionString.split(',')]

df_p =  pd.read_csv('project_selected.csv')
projectList = df_p.project_id.values.tolist()

#df_cc= pd.read_csv('selected/w_selected.csv')
#df_cc = df_cc.reindex(columns = ["project_id", "watcher_date", 'watchers'])
#df_cc.columns = ["project_id", "date", 'watchers']
#df_cc.to_csv('selected/w_selected.csv', index=False)
#print(df_cc.head())

thestart = pd.to_datetime('1999-07-01')
theend = pd.to_datetime('2019-06-30')
date_list = [(pd.to_datetime('1999-07-01') + datetime.timedelta(days=x)).date() for x in range((theend - thestart).days+1)]
date_list = [str(x) for x in date_list]

def makeCSVbyProject(project_id):
    allMeasures = glob.glob("selected/*_selected.csv")
    df_new = pd.DataFrame.from_dict({'date': date_list})
    for filename in allMeasures:
        df = pd.read_csv(filename, index_col=None)
        df = df.loc[df['project_id']==project_id, :]
        df_new = pd.merge(df_new, df.iloc[:,1:3], how='outer', on='date')

    df_new.fillna(0, inplace=True)
    df_new.to_csv(f'csvByProject/{project_id}.csv',index=False)
    #print(df_new.shape)
    #print(df_new.loc[df_new['commits']!=0,:])

def langaugeBarChart():
    a4_dims = (11.7, 8.27)
    smaller_dim = (8, 4.5)
    fig, ax = plt.subplots(figsize=smaller_dim)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    labels = list(df_p.groupby('language').count().index)
    listcountinfosent = df_p.groupby('language').count().loc[:, 'created_at'].values.tolist()
    allcount = list(np.array(listcountinfosent))
    thedict = {}
    for i in range(len(labels)):
        thedict[labels[i]] = allcount[i]
    thesorteddict = sorted(thedict.items(), key=lambda x: x[1], reverse=True)
    labels = [x[0] for x in thesorteddict][:20]
    allcount = [x[1] for x in thesorteddict][:20]
    #print(licensenumberCount)
    x = np.arange(len(labels))
    width = 0.35
    rect1 = ax.bar(x, allcount, width)
    plt.title("Selected Projects in Different Languages")
    plt.xlabel("Language")
    plt.ylabel("Number")
    ax.set_xticks(x)
    ax.set_xticklabels(labels, rotation='vertical')
    ax.legend()
    ax.bar_label(rect1, padding=3)
    fig.tight_layout()
    plt.savefig('lang1.png', bbox_inches='tight')
    plt.show()

def yearBarChart():
    a4_dims = (11.7, 8.27)
    smaller_dim = (8, 4.5)
    fig, ax = plt.subplots(figsize=smaller_dim)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    labels = list(df_p.groupby('year').count().index)
    listcountinfosent = df_p.groupby('year').count().loc[:, 'created_at'].values.tolist()
    allcount = list(np.array(listcountinfosent))
    #thedict = {}
    #for i in range(len(labels)):
    #    thedict[labels[i]] = allcount[i]
    #thesorteddict = sorted(thedict.items(), key=lambda x: x[1], reverse=True)
    #labels = [x[0] for x in thesorteddict]
    #allcount = [x[1] for x in thesorteddict]
    #print(licensenumberCount)
    x = np.arange(len(labels))
    width = 0.35
    rect1 = ax.bar(x, allcount, width)
    plt.title("Selected Projects Created in Different Years")
    plt.xlabel("Year")
    plt.ylabel("Number")
    ax.set_xticks(x)
    ax.set_xticklabels(labels, rotation='vertical')
    ax.legend()
    ax.bar_label(rect1, padding=3)
    fig.tight_layout()
    plt.savefig('year1.png', bbox_inches='tight')
    plt.show()

def func(pct, allvals):
    absolute = int(pct/100.*np.sum(allvals))
    return "{:.1f}%\n({:d})".format(pct, absolute)

df_p['year'] = pd.to_datetime(df_p['created_at']).dt.year

def displayStackoverflowQuestionsStates_matplotlib(thestackdf):
    a4_dims = (11.7, 8.27)
    smaller_dim = (8, 4.5)
    fig, ax = plt.subplots(figsize=smaller_dim)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    df_post = thestackdf.loc[:, ['questions_count']]
    postlist = df_post.loc[:, 'questions_count'].values.tolist()
    postlist = postlist+[0]*(100000-len(postlist))
    rangelist = ['=0','<50', '50-100', '100-200', '200-500', '501-1k', '1k-5k', '5k-10k', '>10k']
    thresholdlist = [0, 50, 100, 200, 500, 1000, 5000, 10000, max(postlist)]
    print(max(postlist))
    countlist = []
    countlist.append(len([x for x in postlist if x == 0]))
    for i in range(len(thresholdlist) - 1):
        countlist.append(len([x for x in postlist if x > thresholdlist[i] and x <= thresholdlist[i + 1]]))
    plot = plt.bar(rangelist, countlist)
    for value in plot:
        height = value.get_height()
        plt.text(value.get_x() + value.get_width() / 2.,
                 1.002 * height, '%d' % int(height), ha='center', va='bottom')
    plt.title("Number of Projects by Number of StackOverflow Qustions")
    plt.xticks(np.arange(len(rangelist)), rangelist)
    plt.xlabel("Number of Questions on StackOverflow")
    plt.ylabel("Number of Projects")
    plt.savefig('stackquestions.png', bbox_inches='tight')
    plt.show()

def makeCSVbyProject2(project_id):
    allMeasures = glob.glob("selected/*_selected_2.csv")
    df_new = pd.DataFrame.from_dict({'date': date_list})
    for filename in allMeasures:
        df = pd.read_csv(filename, index_col=None)
        df = df.loc[df['project_id']==project_id, :]
        df_new = pd.merge(df_new, df.iloc[:,1:3], how='outer', on='date')

    df_new.fillna(0, inplace=True)
    df_new.to_csv(f'{project_id}.csv',index=False)
    #print(df_new.shape)
    #print(df_new.loc[df_new['commits']!=0,:])

#for i in range(1270, 3000):
#    makeCSVbyProject(projectList[i])
#    print(i)

#langaugeBarChart()
#yearBarChart()

#thefile = 'selected/w_selected_2.csv'
#df = pd.read_csv(thefile)
#df=df.reindex(columns=['project_id','date','watchers'])
#df.to_csv(thefile, index=False)


df_oss = pd.read_csv('oss_selected.csv')
osslist = [str(x) for x in df_oss.loc[:,'id'].values.tolist()]

with open('active.txt', 'r', encoding='utf-8') as txtfile:
    activeList = [x.strip('\n') for x in txtfile.readlines()]
with open('inactive.txt', 'r', encoding='utf-8') as txtfile:
    inactiveList = [x.strip('\n') for x in txtfile.readlines()]

thexisting = [x for x in osslist if x in activeList or x in inactiveList]
for item in thexisting:
    print(item)